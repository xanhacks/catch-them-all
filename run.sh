#!/bin/bash

TARGET_DOMAIN="${1:?Please provide a target domain \!}"
USER_AGENT="${2:-nuclei}"

BASE_DIR=$(dirname "$(readlink -f $0)")
BIN_DIR="${BASE_DIR}/bin"
REPORTS_DIR="${BASE_DIR}/reports"
OUT_DIR="${REPORTS_DIR}/${TARGET_DOMAIN}"

AMASS_BINARY="${BIN_DIR}/amass"
HTTPX_BINARY="${BIN_DIR}/httpx"
UNFURL_BINARY="${BIN_DIR}/unfurl"
NUCLEI_BINARY="${BIN_DIR}/nuclei"


log() {
    printf '[%(%d/%m/%Y %H:%M:%S)T]' # date format
    echo " ${1}"
}

run_amass() {
    local domain="${1:?Please provide a domain \!}"
    local output_file="${2:?Please provide an output file \!}"

    log "Running amass on '${domain}'..."
    "${AMASS_BINARY}" enum -passive -d "${domain}" -o "${output_file}"
    log "amass finished on '${domain}'."
}

run_httpx() {
    local domains_file="${1:?Please provide a domains file \!}"
    local output_file="${2:?Please provide an output file \!}"
    local output_domain_file="${3:?Please provide an output domain file \!}"

    log "Running httpx on '${domains_file}' ..."
    "${HTTPX_BINARY}" -silent -list "${domains_file}" -tech-detect -title -status-code \
        -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) / ${USER_AGENT}" -o "${output_file}"
    awk -F' ' '{ print $1 }' "${output_file}" | "${UNFURL_BINARY}" --unique domains | sort > "${output_domain_file}"
    log "httpx finished on '${domains_file}'."
}

run_nuclei() {
    local domains_file="${1:?Please provide a domains file \!}"
    local output_file="${2:?Please provide an output file \!}"

    log "Running nuclei on '${domains_file}' ..."
    "${NUCLEI_BINARY}" -silent -list "${domains_file}" \
        -header "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) / ${USER_AGENT}" -o "${output_file}" # -severity critical,high
    log "nuclei finished on '${domains_file}'."
}

mkdir -p "${OUT_DIR}"
run_amass "${TARGET_DOMAIN}" "${OUT_DIR}/1_amass.txt"
run_httpx "${OUT_DIR}/1_amass.txt" "${OUT_DIR}/2_httpx.txt" "${OUT_DIR}/3_all_domains.txt"
run_nuclei "${OUT_DIR}/3_all_domains.txt" "${OUT_DIR}/4_nuclei.txt"
