# Catch them all

## install.sh

Download the binaries of the following softwares and put it inside the [bin](bin/) folder.

- [Amass](https://github.com/OWASP/Amass)
- [httpx](https://github.com/projectdiscovery/httpx)
- [unfurl](https://github.com/tomnomnom/unfurl)
- [nuclei](https://github.com/projectdiscovery/nuclei)

## run.sh

Run the tools above on the `$TARGET_DOMAIN` with a User-Agent that contains the variable `$USER_AGENT` (optional).