#!/bin/bash

BASE_DIR=$(dirname "$(readlink -f $0)")
BIN_DIR="${BASE_DIR}/bin"

log() {
    printf '[%(%d/%m/%Y %H:%M:%S)T]' # date format
    echo " ${1}"
}

pre_requisites() {
    apt-get update
    apt-get install -y wget unzip
}

install_amass() {
    local tmp_dir="$(mktemp -d)"

    log "Installing amass..."
    wget 'https://github.com/OWASP/Amass/releases/latest/download/amass_linux_amd64.zip' \
        -O "${tmp_dir}/amass_linux_amd64.zip"
    
    (cd "${tmp_dir}" && unzip -q "${tmp_dir}/amass_linux_amd64.zip")
    mv "${tmp_dir}/amass_linux_amd64/amass" "${BIN_DIR}/amass"
    rm -rf "${tmp_dir}/"
    log "amass installed!"
}

install_httpx() {
    local tmp_dir="$(mktemp -d)"

    log "Installing httpx..."
    wget 'https://github.com/projectdiscovery/httpx/releases/download/v1.2.5/httpx_1.2.5_linux_amd64.zip' \
        -O "${tmp_dir}/httpx_linux_amd64.zip"
    
    (cd "${tmp_dir}" && unzip -q "${tmp_dir}/httpx_linux_amd64.zip")
    mv "${tmp_dir}/httpx" "${BIN_DIR}/httpx"
    rm -rf "${tmp_dir}/"
    log "httpx installed!"
}

install_unfurl() {
    local tmp_dir="$(mktemp -d)"

    log "Installing unfurl..."
    wget 'https://github.com/tomnomnom/unfurl/releases/download/v0.4.3/unfurl-linux-amd64-0.4.3.tgz' \
        -O "${tmp_dir}/unfurl-linux-amd64.tgz"
    
    (cd "${tmp_dir}" && tar xzf "${tmp_dir}/unfurl-linux-amd64.tgz")
    mv "${tmp_dir}/unfurl" "${BIN_DIR}/unfurl"
    rm -rf "${tmp_dir}/"
    log "unfurl installed!"  
}

install_nuclei() {
    local tmp_dir="$(mktemp -d)"

    log "Installing nuclei..."
    wget 'https://github.com/projectdiscovery/nuclei/releases/download/v2.8.2/nuclei_2.8.2_linux_amd64.zip' \
        -O "${tmp_dir}/nuclei_linux_amd64.zip"
    
    (cd "${tmp_dir}" && unzip -q "${tmp_dir}/nuclei_linux_amd64.zip")
    mv "${tmp_dir}/nuclei" "${BIN_DIR}/nuclei"
    "${BIN_DIR}/nuclei" # run it to download templates
    rm -rf "${tmp_dir}/"
    log "nuclei installed!"
}

mkdir -p "${BIN_DIR}"
pre_requisites
install_amass
install_httpx
install_unfurl
install_nuclei
